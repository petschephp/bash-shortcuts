#!/bin/bash
source config.sh
source functions.sh

moveFile() {
    cp ${SNIPPETS_DIR}/${HTACCESS_FILE_NAME} $htaccessLocation/${HTACCESS_FILE_NAME}
    if [ $? -ne 0 ]; then
      echo "FAIL"
    else
      echo "${HTACCESS_FILE_NAME} successfully created in $htaccessLocation"
    fi
}

#/Applications/AMPPS/www/yii/web

echo "TYPE in the directory where you want the .htaccess file to be placed: "
read htaccessLocation
echo  ${SNIPPETS_DIR}/${HTACCESS_FILE_NAME} $htaccessLocation/${HTACCESS_FILE_NAME}

FILE=$htaccessLocation/${HTACCESS_FILE_NAME}
# check if file exists
if [ ! -f $FILE ]; then
    echo "FILE DOES NOT EXIST"
    moveFile
else
    echo "FILE EXISTS, do you want to overwrite (y/n)?"
    read overwriteExistingFile

    if [ "$overwriteExistingFile" == "y" ]; then
        moveFile
        exit 0
    else
        echo "File was not copied"
        exit 0
    fi
fi





